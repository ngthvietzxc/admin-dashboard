import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import "./index.css";
import { getBooking } from "../../../redux/action/bookingAction";

function Bookings() {
  const dispatch = useDispatch();
  const bookings = useSelector((state) => state.bookingReducer.bookingData);
  useEffect(() => {
    dispatch(getBooking());
    console.log(bookings);
  }, [dispatch]);

  return (
    <div className="container p-5 ">
      <table className="min-w-full bg-white border border-gray-300">
        <thead className="border-b-2  font-bold">
          <tr>
            <th className="py-2 px-4 border-l border-r text-left ">ID</th>
            <th className="py-2 px-4 border-l border-r text-left ">Mã Phòng</th>
            <th className="py-2 px-4 border-l border-r text-left ">Ngày Đến</th>
            <th className="py-2 px-4 border-l border-r text-left ">Ngày Đi</th>
            <th className="py-2 px-4 border-l border-r text-left ">
              Số Lượng Khách
            </th>
            <th className="py-2 px-4 border-l border-r text-left ">
              Mã Người Dùng
            </th>
            <th className="py-2 px-4 border-l border-r text-left ">Actions</th>
          </tr>
        </thead>
        <tbody>
          {bookings.map((booking) => (
            <tr key={booking.id} className="border-b">
              <td className="py-2 border-l border-r px-4 ">{booking.id}</td>
              <td className="py-2 border-l border-r px-4 ">
                {booking.maPhong}
              </td>
              <td className="py-2 border-l border-r px-4 ">
                {booking.ngayDen}
              </td>
              <td className="py-2 border-l border-r px-4 ">{booking.ngayDi}</td>
              <td className="py-2 border-l border-r px-4 ">
                {booking.soLuongKhach}
              </td>
              <td className="py-2 border-l border-r px-4 ">
                {booking.maNguoiDung}
              </td>
              <td className="py-2 border-l border-r px-4  space-x-2"></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Bookings;
