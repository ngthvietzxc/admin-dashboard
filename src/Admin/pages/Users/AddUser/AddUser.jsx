import React from "react";
import { useDispatch } from "react-redux";
import { Button, DatePicker, Form, Input, Select } from "antd";
import { setAddUser } from "../../../../redux/action/adminAction";
import { Option } from "antd/es/mentions";

export default function AddUser() {
  const dispatch = useDispatch();

  const onFinish = (values) => {
    const { email, password, name, role, gender, birthday } = values;
    const userData = {
      role,
      email,
      password,
      name,
      gender: gender === "true",
      birthday,
    };
    dispatch(setAddUser(userData));
  };

  return (
    <div className="container mx-auto" style={{ width: 450 }}>
      <h2 className="text-center text-2xl font-bold mb-5">Thêm tài khoản</h2>

      <Form
        name="basic"
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 16,
        }}
        className="max-w-md mx-auto"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Tên người dùng"
          name="name"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Giới tính"
          name="gender"
          rules={[
            {
              required: true,
              message: "Please select your gender!",
            },
          ]}
        >
          <Select>
            <Option value="true">Nam</Option>
            <Option value="false">Nữ</Option>
          </Select>
        </Form.Item>

        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Ngày sinh"
          name="birthday"
          rules={[
            {
              required: true,
              message: "Please select your birthday!",
            },
          ]}
        >
          <DatePicker />
        </Form.Item>
        <Form.Item
          label="Quyền truy cập"
          name="role"
          rules={[
            {
              required: true,
              message: "Please select your role!",
            },
          ]}
        >
          <Select>
            <Option value="ADMIN">ADMIN</Option>
            <Option value="USER">USER</Option>
          </Select>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
          <Button
            type="primary"
            htmlType="submit"
            className="bg-gradient-to-r from-blue-500 to-purple-500 hover:from-blue-600 hover:to-purple-600 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
          >
            <span className="mx-auto">Submit</span>
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
