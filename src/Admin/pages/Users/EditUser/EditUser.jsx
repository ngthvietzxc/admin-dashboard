import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getUserById, putUserById } from "../../../../redux/action/adminAction";
import { Form, Input, Button, DatePicker, Select } from "antd";
import moment from "moment";

export default function EditUser() {
  const dispatch = useDispatch();
  const selectedUserId = useSelector(
    (state) => state.adminReducer.selectedUserId
  );
  const userData = useSelector((state) => state.adminReducer.userData);
  const user = userData.find((user) => user.id === selectedUserId);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState("");
  const [birthday, setBirthday] = useState("");
  const [gender, setGender] = useState("");

  useEffect(() => {
    dispatch(getUserById(selectedUserId));
  }, [dispatch, selectedUserId]);

  useEffect(() => {
    if (user) {
      setName(user.name);
      setEmail(user.email);
      setRole(user.role);
      setBirthday(user.birthday ? moment(user.birthday) : null);
      setGender(user.gender);
    }
  }, [user]);

  const handleSaveUser = () => {
    const updatedUserData = {
      name,
      email,
      role,
      birthday,
      gender: gender === "Nam" ? true : false,
    };
    dispatch(putUserById(selectedUserId, updatedUserData));
  };
  return (
    <div className="bg-white" style={{ width: 400 }}>
      <div className="flex w-full justify-center">
        <h2 className="font-bold text-xl pb-5">Thay đổi thông tin</h2>
      </div>

      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        style={{
          maxWidth: 600,
        }}
        onFinish={handleSaveUser}
      >
        <Form.Item label="Tên người dùng">
          <Input value={name} onChange={(e) => setName(e.target.value)} />
        </Form.Item>

        <Form.Item label="Email">
          <Input value={email} onChange={(e) => setEmail(e.target.value)} />
        </Form.Item>
        <Form.Item label="Giới tính">
          <Select value={gender} onChange={(value) => setGender(value)}>
            <Select.Option value="Nam">Nam</Select.Option>
            <Select.Option value="Nữ">Nữ</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Ngày sinh">
          <DatePicker value={birthday} onChange={(date) => setBirthday(date)} />
        </Form.Item>
        <Form.Item label="Quyền truy cập">
          <Select value={role} onChange={(value) => setRole(value)}>
            <Select.Option value="ADMIN">ADMIN</Select.Option>
            <Select.Option value="USER">USER</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            type="primary"
            htmlType="submit"
            className="bg-blue-300  text-white font-semibold rounded"
          >
            Save
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
