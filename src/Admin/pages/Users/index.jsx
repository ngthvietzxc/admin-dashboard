import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import {
  deleteUser,
  getUser,
  searchUser,
  setSelectedUserId,
} from "../../../redux/action/adminAction";
import AddUser from "./AddUser/AddUser";
import EditUser from "./EditUser/EditUser";
import "./index.css";

function Users() {
  // lấy danh sách user
  const dispatch = useDispatch();
  const users = useSelector((state) => state.adminReducer.userData);
  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  // phân chia admin và user
  const [filterRole, setFilterRole] = useState("ADMIN"); // Trạng thái lựa chọn hiển thị
  const filteredUsers = users.filter((user) => user.role === filterRole); // Lọc danh sách người dùng theo vai trò
  const [isAdminList, setIsAdminList] = useState(false); // Trạng thái xác định xem là danh sách ADMIN hay không

  // xóa user
  const handleDeleteUser = (id) => {
    dispatch(deleteUser(id));
  };

  // Tìm kiếm User

  const [searchKeyword, setSearchKeyword] = useState("");
  const onSearch = () => {
    dispatch(searchUser(searchKeyword));
  };

  const handleSearchChange = (e) => {
    const searchValue = e.target.value;
    setSearchKeyword(searchValue);

    // Kiểm tra nếu chuỗi tìm kiếm có chứa chữ in hoa
    const hasUppercase = /[A-Z]/.test(searchValue);
    if (hasUppercase) {
      // Thực hiện tìm kiếm với chuỗi tìm kiếm có chữ in hoa
      dispatch(searchUser(searchValue));
    } else {
      // Nếu không có chữ in hoa, thực hiện tìm kiếm bình thường
      dispatch(searchUser(searchValue.toLowerCase()));
    }
  };
  useEffect(() => {
    if (searchKeyword === "") {
      dispatch(getUser()); // Đặt lại danh sách người dùng ban đầu
    }
  }, [dispatch, searchKeyword]);

  // Phân trang tìm kiếm
  const [currentPage, setCurrentPage] = useState(1);
  const usersPerPage = 10;

  const indexOfLastUser = currentPage * usersPerPage;
  const indexOfFirstUser = indexOfLastUser - usersPerPage;
  const currentUsers = [...filteredUsers]
    .reverse()
    .slice(indexOfFirstUser, indexOfLastUser);

  const totalPages = Math.ceil(filteredUsers.length / usersPerPage);

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };
  // chọn Id để edit user
  const handleEditUser = (id) => {
    dispatch(setSelectedUserId(id));
    setShowEditUser(true);
  };
  // hiển thị add admin
  const [showAddUser, setShowAddUser] = useState(false); // Trạng thái hiển thị AddUser component

  const handleShowAddUser = () => {
    setShowAddUser(true);
  };
  // tat add
  const handleCloseAddUser = () => {
    setShowAddUser(false);
  };
  //
  const [showEditUser, setShowEditUser] = useState(false);
  const handleCloseEditUser = () => {
    setShowEditUser(false);
  };
  return (
    <div className="container p-5 ">
      <div className="container flex  space-x-1 pb-10 ">
        <div className="w-1/2 flex justify-center">
          <button
            className={`${
              isAdminList ? "bg-blue-700" : "bg-blue-500 hover:bg-blue-700 "
            } text-white py-2 w-full font-semibold`}
            onClick={() => {
              setFilterRole("ADMIN");
              setIsAdminList(true);
            }}
          >
            Danh sách Admin
          </button>
        </div>
        <div className="w-1/2 flex justify-center">
          <button
            className={`${
              !isAdminList ? "bg-blue-700" : "bg-blue-500 hover:bg-blue-700"
            } text-white py-2 w-full font-semibold `}
            onClick={() => {
              setFilterRole("USER");
              setIsAdminList(false);
            }}
          >
            Danh sách người dùng
          </button>
        </div>
      </div>
      <div className="pb-5">
        <input
          value={searchKeyword}
          onChange={handleSearchChange}
          onSearch={onSearch}
          type="text"
          className="w-full py-1 pr-10 pl-4 border border-gray-300 rounded-lg focus:outline-none focus:border-blue-500"
          placeholder="Tìm kiếm người dùng"
        />
      </div>
      <div className="p-5 bg-white">
        <div className="pb-5">
          <button
            onClick={handleShowAddUser}
            className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-3 font-semibold rounded"
          >
            Thêm tài khoản
          </button>
        </div>

        <table className="min-w-full border border-gray-300">
          <thead className="bg-gray-500 text-white">
            <tr>
              <th className="py-3 px-4 border-r">ID</th>
              <th className="py-3 px-4 border-r">Name</th>
              <th className="py-3 px-4 border-r">Email</th>
              <th className="py-3 px-4 border-r">Birthday</th>
              <th className="py-3 px-4 border-r">Gender</th>
              <th className="py-3 px-4">Actions</th>
            </tr>
          </thead>
          <tbody>
            {currentUsers.map((user) => (
              <tr key={user.id} className="border-b">
                <td className="py-4 px-4 border-r">{user.id}</td>
                <td className="py-4 px-4 border-r">{user.name}</td>
                <td className="py-4 px-4 border-r">{user.email}</td>
                <td className="py-4 px-4 border-r">
                  {user.birthday
                    ? moment(user.birthday).format("DD/MM/YYYY")
                    : "-"}
                </td>
                <td className="py-4 px-4 border-r">
                  {user.gender ? "Nam" : "Nữ"}
                </td>
                <td className="py-4 px-4 space-x-2">
                  <button
                    className="bg-blue-500 hover:bg-blue-700 text-white py-1 px-3 font-semibold rounded"
                    onClick={() => handleDeleteUser(user.id)}
                  >
                    Delete
                  </button>
                  <button
                    className="bg-blue-500 hover:bg-blue-700 text-white py-1 px-3 font-semibold rounded"
                    onClick={() => handleEditUser(user.id)}
                  >
                    Edit
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>

        <div className="flex justify-center mt-4">
          {Array.from({ length: totalPages }, (_, index) => (
            <button
              key={index}
              onClick={() => paginate(index + 1)}
              className={`border border-gray-200 rounded px-2 py-1 mx-1 focus:outline-none focus:border-black-500 hover:bg-gray-200 ${
                currentPage === index + 1 ? "bg-gray-300" : ""
              }`}
            >
              {index + 1}
            </button>
          ))}
        </div>
      </div>

      {showEditUser && (
        <div className="overlay">
          <div className="add-user-modal">
            <button
              className="close-button"
              onClick={handleCloseEditUser}
            ></button>
            <EditUser />
          </div>
        </div>
      )}
      {showAddUser && (
        <div className="overlay">
          <div className="add-user-modal">
            <button
              className="close-button"
              onClick={handleCloseAddUser}
            ></button>
            <AddUser />
          </div>
        </div>
      )}
    </div>
  );
}

export default Users;
