import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { deleteRoom, getRoom } from "../../../redux/action/roomAction";

import { NavLink } from "react-router-dom";

function Rooms() {
  const dispatch = useDispatch();
  const rooms = useSelector((state) => state.roomReducer.roomData);
  // Lấy danh sách phòng
  useEffect(() => {
    dispatch(getRoom());
  }, [dispatch]);
  // Phân trang danh sách phòng
  const [currentPage, setCurrentPage] = useState(1);
  const usersPerPage = 4;
  const totalPages = Math.ceil(rooms.length / usersPerPage);
  const lastIndex = currentPage * usersPerPage;
  const firstIndex = lastIndex - usersPerPage;
  const currentItems = rooms.slice(firstIndex, lastIndex);
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };
  // Xóa Room
  const handleDeleteRoom = (id) => {
    dispatch(deleteRoom(id));
  };

  return (
    <div>
      <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-6 p-10">
        {currentItems.map((room) => (
          <div key={room.id} className="p-6 bg-white rounded-lg shadow-lg">
            <img
              src={room.hinhAnh}
              alt={room.tenPhong}
              className="w-full h-48 object-cover rounded-md mb-4"
            />
            <h4 className="text-xl font-semibold mb-2">{room.tenPhong}</h4>
            <div className="flex justify-end items-center space-x-3">
              <NavLink
                to={`/room/${room.id}`}
                className="bg-gradient-to-r from-blue-500 to-blue-700 hover:from-blue-600 hover:to-blue-800 text-white py-2 px-4 rounded shadow-sm transition-all duration-300 text-sm"
              >
                Xem chi tiết
              </NavLink>
              <button
                className="bg-gradient-to-r from-red-500 to-red-700 hover:from-red-600 hover:to-red-800 text-white py-2 px-4 rounded shadow-sm transition-all duration-300 text-sm"
                onClick={() => handleDeleteRoom(room.id)}
              >
                Xóa phòng
              </button>
            </div>
          </div>
        ))}
      </div>
      <div className="flex justify-center mt-4">
        {Array.from({ length: totalPages }, (_, index) => (
          <button
            key={index}
            onClick={() => paginate(index + 1)}
            className={`border border-gray-200 rounded px-3 py-2 mx-1 focus:outline-none focus:border-black-500 hover:bg-gray-200 ${
              currentPage === index + 1 ? "bg-gray-300" : ""
            }`}
          >
            {index + 1}
          </button>
        ))}
      </div>
    </div>
  );
}

export default Rooms;
