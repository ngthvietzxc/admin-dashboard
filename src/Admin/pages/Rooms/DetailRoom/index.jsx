import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getRoomById } from "./../../../../redux/action/roomAction";
import EditRoom from "../EditRoom";

export default function DetailRoom() {
  let params = useParams();
  const dispatch = useDispatch();

  const room = useSelector((state) => {
    const rooms = state.roomReducer.roomData;
    return rooms.find((room) => room.id === parseInt(params.id));
  });

  useEffect(() => {
    dispatch(getRoomById(parseInt(params.id)));
  }, [dispatch, params.id]);
  // Edit
  const [showEditRoom, setShowEditRoom] = useState(false); // Thêm state "showEditRoom" và hàm "setShowEditRoom"
  const handleEditClick = () => {
    setShowEditRoom(true);
  };
  return (
    <div className="container mx-auto py-8 px-4 sm:px-6 lg:px-8">
      {room && (
        <div className="bg-white shadow-lg rounded-lg p-8">
          <div className="flex flex-col md:flex-row mb-4">
            <h2 className="text-3xl font-bold">{room.tenPhong}</h2>
          </div>
          <div className="flex p-8">
            <img
              src={room.hinhAnh}
              alt={room.tenPhong}
              className="rounded-lg shadow-md w-full"
            />
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
            <div>
              <div className="mb-2 text-gray-600">
                Guests: {room.khach} | Bedrooms: {room.phongNgu} | Beds:{" "}
                {room.giuong} | Bathrooms: {room.phongTam}
              </div>
              <div className="mb-2 text-gray-600">Location: {room.maViTri}</div>
              <div className="text-xl font-semibold mb-2">
                Price: ${room.giaTien} per night
              </div>
              <div className="mb-2 text-gray-600">Features:</div>
              <ul className="list-disc list-inside ml-4">
                {room.mayGiat && <li className="text-green-600">Washer</li>}
                {room.banLa && <li className="text-green-600">Dryer</li>}
                {room.tivi && <li className="text-green-600">TV</li>}
                {room.dieuHoa && (
                  <li className="text-green-600">Air Conditioning</li>
                )}
                {room.wifi && <li className="text-green-600">Wi-Fi</li>}
                {room.bep && <li className="text-green-600">Kitchen</li>}
                {room.doXe && <li className="text-green-600">Parking</li>}
                {room.hoBoi && <li className="text-green-600">Pool</li>}
                {room.banUi && <li className="text-green-600">Iron</li>}
              </ul>
            </div>
            <div>
              <h3 className="text-lg font-semibold mb-2">Description</h3>
              <p>{room.moTa}</p>
            </div>
          </div>
          <div className="mt-8">
            <h3 className="text-lg font-semibold mb-2">
              Additional Information
            </h3>
            <table className="min-w-full divide-y divide-gray-200">
              <tbody className="bg-white divide-y divide-gray-200">
                <tr>
                  <td className="py-2 pr-4 font-semibold">Guests:</td>
                  <td className="py-2">{room.khach}</td>
                </tr>
                <tr>
                  <td className="py-2 pr-4 font-semibold">Bedrooms:</td>
                  <td className="py-2">{room.phongNgu}</td>
                </tr>
                <tr>
                  <td className="py-2 pr-4 font-semibold">Beds:</td>
                  <td className="py-2">{room.giuong}</td>
                </tr>
                <tr>
                  <td className="py-2 pr-4 font-semibold">Bathrooms:</td>
                  <td className="py-2">{room.phongTam}</td>
                </tr>
                <tr>
                  <td className="py-2 pr-4 font-semibold">Location:</td>
                  <td className="py-2">{room.maViTri}</td>
                </tr>
                <tr>
                  <td className="py-2 pr-4 font-semibold">Price per night:</td>
                  <td className="py-2">${room.giaTien}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <button
            onClick={handleEditClick}
            className="bg-blue-500 text-white py-2 px-4 rounded mt-4"
          >
            Edit
          </button>

          {showEditRoom && <EditRoom />}
        </div>
      )}
    </div>
  );
}
