import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Form, Input, InputNumber, Checkbox, Button } from "antd";
import { getRoomById, putRoomById } from "../../../../redux/action/roomAction";

export default function EditRoom() {
  let params = useParams();
  const dispatch = useDispatch();

  const room = useSelector((state) => {
    const rooms = state.roomReducer.roomData;
    return rooms.find((room) => room.id === parseInt(params.id));
  });

  useEffect(() => {
    dispatch(getRoomById(parseInt(params.id)));
  }, [dispatch, params.id]);

  const [tenPhong, setTenPhong] = useState("");
  const [khach, setKhach] = useState(0);
  const [phongNgu, setPhongNgu] = useState(0);
  const [giuong, setGiuong] = useState(0);
  const [phongTam, setPhongTam] = useState(0);
  const [moTa, setMoTa] = useState("");
  const [giaTien, setGiaTien] = useState(0);
  const [mayGiat, setMayGiat] = useState(false);
  const [banLa, setBanLa] = useState(false);
  const [tivi, setTivi] = useState(false);
  const [dieuHoa, setDieuHoa] = useState(false);
  const [wifi, setWifi] = useState(false);
  const [bep, setBep] = useState(false);
  const [doXe, setDoXe] = useState(false);
  const [hoBoi, setHoBoi] = useState(false);
  const [banUi, setBanUi] = useState(false);
  const [maViTri, setMaViTri] = useState(0);
  const [hinhAnh, setHinhAnh] = useState("");

  useEffect(() => {
    if (room) {
      setTenPhong(room.tenPhong);
      setKhach(room.khach);
      setPhongNgu(room.phongNgu);
      setGiuong(room.giuong);
      setPhongTam(room.phongTam);
      setMoTa(room.moTa);
      setGiaTien(room.giaTien);
      setMayGiat(room.mayGiat);
      setBanLa(room.banLa);
      setTivi(room.tivi);
      setDieuHoa(room.dieuHoa);
      setWifi(room.wifi);
      setBep(room.bep);
      setDoXe(room.doXe);
      setHoBoi(room.hoBoi);
      setBanUi(room.banUi);
      setMaViTri(room.maViTri);
      setHinhAnh(room.hinhAnh);
    }
  }, [room]);

  const handleSaveRoom = () => {
    const updatedRoomData = {
      tenPhong,
      khach,
      phongNgu,
      giuong,
      phongTam,
      moTa,
      giaTien,
      mayGiat,
      banLa,
      tivi,
      dieuHoa,
      wifi,
      bep,
      doXe,
      hoBoi,
      banUi,
      maViTri,
      hinhAnh,
    };
    dispatch(putRoomById(parseInt(params.id), updatedRoomData));
  };

  return (
    <div>
      <Form>
        <Form.Item label="Tên phòng">
          <Input
            value={tenPhong}
            onChange={(e) => setTenPhong(e.target.value)}
          />
        </Form.Item>
        <Form.Item label="Số khách">
          <InputNumber value={khach} onChange={(value) => setKhach(value)} />
        </Form.Item>
        <Form.Item label="Số phòng ngủ">
          <InputNumber
            value={phongNgu}
            onChange={(value) => setPhongNgu(value)}
          />
        </Form.Item>
        <Form.Item label="Số giường">
          <InputNumber value={giuong} onChange={(value) => setGiuong(value)} />
        </Form.Item>
        <Form.Item label="Số phòng tắm">
          <InputNumber
            value={phongTam}
            onChange={(value) => setPhongTam(value)}
          />
        </Form.Item>
        <Form.Item label="Mô tả">
          <Input.TextArea
            value={moTa}
            onChange={(e) => setMoTa(e.target.value)}
          />
        </Form.Item>
        <Form.Item label="Giá tiền">
          <InputNumber
            value={giaTien}
            onChange={(value) => setGiaTien(value)}
          />
        </Form.Item>
        <Form.Item label="Máy giặt">
          <Checkbox
            checked={mayGiat}
            onChange={(e) => setMayGiat(e.target.checked)}
          >
            Có máy giặt
          </Checkbox>
        </Form.Item>
        <Form.Item label="Bàn là">
          <Checkbox
            checked={banLa}
            onChange={(e) => setBanLa(e.target.checked)}
          >
            Có bàn là
          </Checkbox>
        </Form.Item>
        <Form.Item label="TV">
          <Checkbox checked={tivi} onChange={(e) => setTivi(e.target.checked)}>
            Có TV
          </Checkbox>
        </Form.Item>
        <Form.Item label="Điều hòa">
          <Checkbox
            checked={dieuHoa}
            onChange={(e) => setDieuHoa(e.target.checked)}
          >
            Có điều hòa
          </Checkbox>
        </Form.Item>
        <Form.Item label="Wifi">
          <Checkbox checked={wifi} onChange={(e) => setWifi(e.target.checked)}>
            Có wifi
          </Checkbox>
        </Form.Item>
        <Form.Item label="Bếp">
          <Checkbox checked={bep} onChange={(e) => setBep(e.target.checked)}>
            Có bếp
          </Checkbox>
        </Form.Item>
        <Form.Item label="Đỗ xe">
          <Checkbox checked={doXe} onChange={(e) => setDoXe(e.target.checked)}>
            Có chỗ đỗ xe
          </Checkbox>
        </Form.Item>
        <Form.Item label="Hồ bơi">
          <Checkbox
            checked={hoBoi}
            onChange={(e) => setHoBoi(e.target.checked)}
          >
            Có hồ bơi
          </Checkbox>
        </Form.Item>
        <Form.Item label="Bàn ủi">
          <Checkbox
            checked={banUi}
            onChange={(e) => setBanUi(e.target.checked)}
          >
            Có bàn ủi
          </Checkbox>
        </Form.Item>
        <Form.Item label="Mã vị trí">
          <InputNumber
            value={maViTri}
            onChange={(value) => setMaViTri(value)}
          />
        </Form.Item>
        <Form.Item label="Hình ảnh">
          <Input value={hinhAnh} onChange={(e) => setHinhAnh(e.target.value)} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" onClick={handleSaveRoom}>
            Save
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
