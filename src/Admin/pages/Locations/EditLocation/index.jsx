import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Input } from "antd";

import {
  getLocationById,
  putLocationById,
} from "../../../../redux/action/locationAction";

export default function EditLocation() {
  const dispatch = useDispatch();
  const selectedLocationId = useSelector(
    (state) => state.locationReducer.selectedLocationId
  );
  const locationData = useSelector(
    (state) => state.locationReducer.locationData
  );
  const location = locationData.find(
    (location) => location.id === selectedLocationId
  );

  const [tenViTri, setTenViTri] = useState("");
  const [tinhThanh, setTinhThanh] = useState("");
  const [quocGia, setQuocGia] = useState("");
  const [hinhAnh, setHinhAnh] = useState("");

  useEffect(() => {
    dispatch(getLocationById(selectedLocationId));
  }, [dispatch, selectedLocationId]);

  useEffect(() => {
    if (location) {
      setTenViTri(location.tenViTri);
      setTinhThanh(location.tinhThanh);
      setQuocGia(location.quocGia);
      setHinhAnh(location.hinhAnh);
    }
  }, [location]);

  const handleSaveLocation = () => {
    const updatedLocationData = {
      tenViTri,
      tinhThanh,
      quocGia,
      hinhAnh,
    };
    dispatch(putLocationById(selectedLocationId, updatedLocationData));
  };

  return (
    <div className="container mx-auto" style={{ width: 450 }}>
      <h2 className="text-center text-2xl font-bold mb-5">Thêm tài khoản</h2>

      <Form
        name="basic"
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 16,
        }}
        className="max-w-md mx-auto"
        initialValues={{
          remember: true,
        }}
        onFinish={handleSaveLocation}
        autoComplete="off"
      >
        <Form.Item label="Tên Vị Trí">
          <Input
            value={tenViTri}
            onChange={(e) => setTenViTri(e.target.value)}
          />
        </Form.Item>
        <Form.Item label="Quốc Gia">
          <Input value={quocGia} onChange={(e) => setQuocGia(e.target.value)} />
        </Form.Item>
        <Form.Item label="Tỉnh Thành">
          <Input
            value={tinhThanh}
            onChange={(e) => setTinhThanh(e.target.value)}
          />
        </Form.Item>

        <Form.Item label="Hình Ảnh">
          <Input value={hinhAnh} onChange={(e) => setHinhAnh(e.target.value)} />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
          <Button
            type="primary"
            htmlType="submit"
            className="bg-gradient-to-r from-blue-500 to-purple-500 hover:from-blue-600 hover:to-purple-600 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
          >
            <span className="mx-auto">Submit</span>
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
