import React from "react";
import { useDispatch } from "react-redux";
import { Button, Form, Input } from "antd";

import { setAddLocation } from "../../../../redux/action/locationAction";

export default function AddLocation() {
  const dispatch = useDispatch();

  const onFinish = (values) => {
    const { tenViTri, tinhThanh, quocGia, hinhAnh } = values;
    const locationData = {
      hinhAnh,
      tenViTri,
      tinhThanh,
      quocGia,
    };
    dispatch(setAddLocation(locationData));
  };

  return (
    <div className="container mx-auto" style={{ width: 450 }}>
      <h2 className="text-center text-2xl font-bold mb-5">Thêm tài khoản</h2>

      <Form
        name="basic"
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 16,
        }}
        className="max-w-md mx-auto"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Quốc Gia"
          name="quocGia"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Tên Vị Trí"
          name="tenViTri"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Tỉnh Thành"
          name="tinhThanh"
          rules={[
            {
              required: true,
              message: "Please input your tinhThanh!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Hình Ảnh"
          name="hinhAnh"
          rules={[
            {
              required: true,
              message: "Please select your role!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
          <Button
            type="primary"
            htmlType="submit"
            className="bg-gradient-to-r from-blue-500 to-purple-500 hover:from-blue-600 hover:to-purple-600 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
          >
            <span className="mx-auto">Submit</span>
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
