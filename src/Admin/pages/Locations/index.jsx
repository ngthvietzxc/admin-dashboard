import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import "./index.css";
import {
  deleteLocation,
  getLocation,
  setSelectedLocationId,
} from "../../../redux/action/locationAction";
import AddLocation from "./AddLocation";
import EditLocation from "./EditLocation/index";

function Locations() {
  const dispatch = useDispatch();
  const locations = useSelector((state) => state.locationReducer.locationData);
  useEffect(() => {
    dispatch(getLocation());
  }, [dispatch]);

  const [currentPage, setCurrentPage] = useState(1);
  const usersPerPage = 5;
  const totalPages = Math.ceil(locations.length / usersPerPage);
  const lastIndex = currentPage * usersPerPage;
  const firstIndex = lastIndex - usersPerPage;
  const currentItems = locations.slice(firstIndex, lastIndex);
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleDeleteLocation = (id) => {
    dispatch(deleteLocation(id));
  };

  const [showAddLocation, setShowAddLocation] = useState(false);

  const handleShowAddLocation = () => {
    setShowAddLocation(true);
  };
  const handleCloseAddLocation = () => {
    setShowAddLocation(false);
  };
  const [showEditLocation, setShowEditLocation] = useState(false);
  const handleCloseEditLocation = () => {
    setShowEditLocation(false);
  };
  // chọn Id để edit user
  const handleEditLocation = (id) => {
    dispatch(setSelectedLocationId(id));
    setShowEditLocation(true);
  };
  return (
    <div className="container p-5">
      <div className="pb-5">
        <button
          onClick={handleShowAddLocation}
          className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 font-semibold rounded"
        >
          Thêm vị trí
        </button>
      </div>
      <div className="p-5">
        <table className="w-full border">
          <thead>
            <tr className="bg-gray-200">
              <th className="py-3 px-4 border-r text-left ">ID</th>
              <th className="py-3 px-4 border-r text-left ">Vị trí</th>
              <th className="py-3 px-4 border-r text-left ">Tỉnh thành</th>
              <th className="py-3 px-4 border-r text-left ">Quốc gia</th>
              <th className="py-3 px-4 border-r text-left ">Hình ảnh</th>
              <th className="py-3 px-4 text-left">Actions</th>
            </tr>
          </thead>
          <tbody>
            {currentItems.map((location) => (
              <tr key={location.id} className="bg-white">
                <td className="py-4 px-4 border-r border-b">{location.id}</td>
                <td className="py-4 px-4 border-r border-b">
                  {location.tenViTri}
                </td>
                <td className="py-4 px-4 border-r border-b">
                  {location.tinhThanh}
                </td>
                <td className="py-4 px-4 border-r border-b">
                  {location.quocGia}
                </td>
                <td className="py-4 px-4 border-r border-b">
                  <img
                    src={location.hinhAnh}
                    alt=""
                    className="w-40 h-40 object-cover rounded"
                  />
                </td>
                <td className="py-4 px-4 border-b">
                  <button
                    className="bg-red-500 hover:bg-red-700 text-white py-1 px-3 font-semibold rounded"
                    onClick={() => handleDeleteLocation(location.id)}
                  >
                    Delete
                  </button>
                  <button
                    className="bg-blue-500 hover:bg-blue-700 text-white py-1 px-3 font-semibold rounded"
                    onClick={() => handleEditLocation(location.id)}
                  >
                    Edit
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className="flex justify-center mt-4">
          {Array.from({ length: totalPages }, (_, index) => (
            <button
              key={index}
              onClick={() => paginate(index + 1)}
              className={`border border-gray-200 rounded px-3 py-2 mx-1 focus:outline-none focus:border-black-500 hover:bg-gray-200 ${
                currentPage === index + 1 ? "bg-gray-300" : ""
              }`}
            >
              {index + 1}
            </button>
          ))}
        </div>
      </div>
      {showEditLocation && (
        <div className="overlay">
          <div className="add-user-modal">
            <button
              className="close-button"
              onClick={handleCloseEditLocation}
            ></button>
            <EditLocation />
          </div>
        </div>
      )}
      {showAddLocation && (
        <div className="overlay">
          <div className="add-user-modal">
            <button
              className="close-button"
              onClick={handleCloseAddLocation}
            ></button>
            <AddLocation />
          </div>
        </div>
      )}
    </div>
  );
}

export default Locations;
