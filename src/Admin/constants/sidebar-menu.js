const sidebar_menu = [
  {
    id: 1,
    path: "/users",
    title: "Quản lý người dùng",
  },
  {
    id: 2,
    path: "/locations",
    title: "Quản lý thông tin vị trí",
  },
  {
    id: 3,
    path: "/rooms",
    title: "Quản lý thông tin phòng",
  },
  {
    id: 4,
    path: "/bookings",
    title: "Quản lý đặt phòng",
  },
];

export default sidebar_menu;
