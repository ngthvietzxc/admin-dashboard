import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const bookingService = {
  getBooking: () => {
    return axios({
      url: `${BASE_URL}/api/dat-phong`,
      method: "GET",
      headers: configHeaders(),
    });
  },
};
