import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const adminService = {
  getUser: () => {
    return axios({
      url: `${BASE_URL}/api/users`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  deleteUser: (id) => {
    return axios({
      url: `${BASE_URL}/api/users?id=${id}`,
      method: "DELETE",
      headers: configHeaders(),
    });
  },
  addUser: (userAdd) => {
    return axios({
      url: `${BASE_URL}/api/users`,
      method: "POST",
      data: userAdd,
      headers: configHeaders(),
    });
  },
  searchUser: (keyword) => {
    return axios({
      url: `${BASE_URL}/api/users/search/${keyword}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  getUserById: (id) => {
    return axios({
      url: `${BASE_URL}/api/users?id=${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },

  putUserById: (id, userData) => {
    return axios({
      url: `${BASE_URL}/api/users/${id}`,
      method: "PUT",
      data: userData,
      headers: configHeaders(),
    });
  },
};
