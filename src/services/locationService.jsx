import axios from "axios";
import { BASE_URL, configHeaders } from "./config";
import { localUserService } from "./localServices";

export const locationService = {
  getLocation: () => {
    return axios({
      url: `${BASE_URL}/api/vi-tri`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  deleteLocation: (id) => {
    const token = localUserService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/vi-tri/${id}`,
      method: "DELETE",
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  addLocation: (locationAdd) => {
    const token = localUserService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/vi-tri`,
      method: "POST",
      data: locationAdd,
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  getLocationById: (id) => {
    return axios({
      url: `${BASE_URL}/api/vi-tri?id=${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },

  putLocationById: (id, locationData) => {
    const token = localUserService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/vi-tri/${id}`,
      method: "PUT",
      data: locationData,
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
};
