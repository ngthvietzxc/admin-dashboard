import axios from "axios";
import { BASE_URL, configHeaders } from "./config";
import { localUserService } from "./localServices";

export const roomService = {
  getRoom: () => {
    return axios({
      url: `${BASE_URL}/api/phong-thue`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  deleteRoom: (id) => {
    const token = localUserService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/phong-thue/${id}`,
      method: "DELETE",
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  addRoom: (roomAdd) => {
    const token = localUserService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/phong-thue`,
      method: "POST",
      data: roomAdd,
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  getRoomById: (id) => {
    return axios({
      url: `${BASE_URL}/api/phong-thue?${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  putRoomById: (id, roomData) => {
    const token = localUserService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/phong-thue/${id}`,
      method: "PUT",
      data: roomData,
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
};
