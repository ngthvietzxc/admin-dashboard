import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import "./App.css";

import sidebar_menu from "./Admin/constants/sidebar-menu";

import Users from "./Admin/pages/Users";

import SideBar from "./Admin/components/Sidebar/index";
import Header from "./Admin/components/Header";

import Locations from "./Admin/pages/Locations";
import Login from "./Login/index";
import SignUp from "./SignUp/index";
import Rooms from "./Admin/pages/Rooms/index";
import Bookings from "./Admin/pages/Bookings/index";
import DetailRoom from "./Admin/pages/Rooms/DetailRoom";

function App() {
  return (
    <div>
      <Router>
        <div>
          <Header />
          <div className="dashboard-container">
            <SideBar menu={sidebar_menu} />
            <div className="dashboard-body pl-5">
              <Routes>
                <Route exact path="/login" element={<Login />} />
                <Route exact path="/sign-up" element={<SignUp />} />
                <Route exact path="/users" element={<Users />} />
                <Route exact path="/locations" element={<Locations />} />
                <Route exact path="/rooms" element={<Rooms />} />
                <Route exact path="/bookings" element={<Bookings />} />
                <Route exact path="/room/:id" element={<DetailRoom />} />
              </Routes>
            </div>
          </div>
        </div>
      </Router>
    </div>
  );
}

export default App;
