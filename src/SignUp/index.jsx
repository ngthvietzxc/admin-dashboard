import React from "react";
import { Form, Input, Button, Checkbox, DatePicker, Select } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUserRegisterAction } from "../redux/action/userAction";

const { Option } = Select;

export default function SignUp() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    const { email, password, name, role, gender, birthday } = values;

    const formRegister = {
      role,
      email,
      password,
      name,
      gender: gender === "true",
      birthday,
    };
    navigate(`/login`);
    dispatch(setUserRegisterAction(formRegister));
  };
  return (
    <div className="min-h-screen bg-gradient-to-r from-purple-600 via-indigo-600 to-blue-600 flex items-center justify-center">
      <div className="max-w-md w-full bg-white p-8 rounded shadow">
        <h1 className="text-3xl font-bold mb-6 text-gray-800">Sign Up</h1>
        <Form name="signup-form" onFinish={onFinish}>
          <Form.Item
            name="name"
            rules={[{ required: true, message: "Please input your name!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Name"
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Please input your email!" },
              { type: "email", message: "Please enter a valid email!" },
            ]}
          >
            <Input
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="Password"
            />
          </Form.Item>

          <Form.Item
            name="birthday"
            rules={[
              { required: true, message: "Please select your birthday!" },
            ]}
          >
            <DatePicker className="w-full" placeholder="Birthday" />
          </Form.Item>
          <Form.Item
            name="gender"
            rules={[{ required: true, message: "Please select your gender!" }]}
          >
            <Select placeholder="Select Gender">
              <Option value="male">Nam</Option>
              <Option value="female">Nữ</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="role"
            rules={[{ required: true, message: "Please select your role!" }]}
          >
            <Select placeholder="Select Role">
              <Option value="user">USER</Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <Form.Item name="agreement" valuePropName="checked" noStyle>
              <Checkbox>
                I have read and agree to the{" "}
                <a className="text-blue-600 hover:underline" href="/">
                  Terms of Service
                </a>
              </Checkbox>
            </Form.Item>
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="w-full bg-gradient-to-r from-purple-700 via-indigo-700 to-blue-700 hover:from-purple-800 hover:via-indigo-800 hover:to-blue-800"
            >
              Sign Up
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
