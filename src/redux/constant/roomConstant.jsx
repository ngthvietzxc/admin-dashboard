export const GET_ROOM = "GET_ROOM";
export const DELETE_ROOM = "DELETE_ROOM";
export const GET_ROOM_BY_ID = "GET_ROOM_BY_ID";
export const PUT_ROOM_BY_ID = "PUT_ROOM_BY_ID";
