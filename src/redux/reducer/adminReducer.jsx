import {
  DELETE_USER,
  GET_USER,
  GET_USER_BY_ID,
  PAGINATED_USERS,
  PUT_USER_BY_ID,
  SEARCH_USER,
  SET_SELECTED_USER_ID,
} from "../constant/adminConstant";

const initialState = {
  userData: [],
  pagination: {
    pageIndex: 1,
    pageSize: 5,
  },
  selectedUserId: null,
};

export const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        ...state,
        userData: action.payload,
      };
    case DELETE_USER:
      return {
        ...state,
        userData: state.userData.filter((user) => user.id !== action.payload),
      };
    case SEARCH_USER:
      return {
        ...state,
        userData: action.payload,
      };
    case PAGINATED_USERS:
      return {
        ...state,
        userData: action.payload,
        pagination: {
          ...state.pagination,
          pageIndex: action.pageIndex,
          pageSize: action.pageSize,
        },
      };
    case GET_USER_BY_ID:
      return {
        ...state,
        userData: action.payload,
      };
    case SET_SELECTED_USER_ID:
      return {
        ...state,
        selectedUserId: action.payload,
      };
    case PUT_USER_BY_ID:
      return {
        ...state,
        userData: action.payload,
      };
    default:
      return state;
  }
};
