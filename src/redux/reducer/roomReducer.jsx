import {
  DELETE_ROOM,
  GET_ROOM,
  GET_ROOM_BY_ID,
  PUT_ROOM_BY_ID,
  SET_SELECTED_ROOM_ID,
} from "./../constant/roomConstant";

const initialState = {
  roomData: [],
};
export const roomReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ROOM:
      return {
        ...state,
        roomData: action.payload,
      };
    case DELETE_ROOM:
      return {
        ...state,
        roomData: state.roomData.filter((room) => room.id !== action.payload),
      };
    case GET_ROOM_BY_ID:
      return {
        ...state,
        roomData: action.payload,
      };

    case PUT_ROOM_BY_ID:
      return {
        ...state,
        roomData: action.payload,
      };
    default:
      return state;
  }
};
