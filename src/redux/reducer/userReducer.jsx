import { localUserService } from "../../services/localServices";
import { USER_LOGIN, USER_REGISTER } from "../constant/userConstant";

const initialState = {
  userInfo: localUserService.get(),
  userRegisterInfo: null,
  token: null,
};

let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN:
      state.userInfo = payload;
      state.token = payload.token;
      localUserService.set({ token: payload.token });
      return { ...state };
    case USER_REGISTER:
      state.userRegisterInfo = payload;
      return { ...state };
    default:
      return state;
  }
};
export default userReducer;
