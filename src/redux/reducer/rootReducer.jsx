import { combineReducers } from "redux";
import { adminReducer } from "./adminReducer";
import { locationReducer } from "./locationReducer";
import { roomReducer } from "./roomReducer";
import { bookingReducer } from "./bookingReducer";
import userReducer from "./userReducer";

export const rootReducer = combineReducers({
  adminReducer,
  locationReducer,
  roomReducer,
  bookingReducer,
  userReducer,
});
