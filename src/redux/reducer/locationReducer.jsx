import {
  DELETE_LOCATION,
  GET_LOCATION,
  GET_LOCATION_BY_ID,
  PUT_LOCATION_BY_ID,
  SET_SELECTED_LOCATION_ID,
} from "../constant/locationConstant";

const initialState = {
  locationData: [],
};
export const locationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LOCATION:
      return {
        ...state,
        locationData: action.payload,
      };
    case DELETE_LOCATION:
      return {
        ...state,
        locationData: state.locationData.filter(
          (location) => location.id !== action.payload
        ),
      };
    case GET_LOCATION_BY_ID:
      return {
        ...state,
        locationData: action.payload,
      };
    case SET_SELECTED_LOCATION_ID:
      return {
        ...state,
        selectedLocationId: action.payload,
      };
    case PUT_LOCATION_BY_ID:
      return {
        ...state,
        locationData: action.payload,
      };
    default:
      return state;
  }
};
