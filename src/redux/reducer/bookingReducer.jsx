import { GET_BOOKING } from "./../constant/bookingConstant";

const initialState = {
  bookingData: [],
};
export const bookingReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BOOKING:
      return {
        ...state,
        bookingData: action.payload,
      };
    default:
      return state;
  }
};
