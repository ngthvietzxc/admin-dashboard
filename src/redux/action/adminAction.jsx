import { adminService } from "../../services/adminService";
import {
  DELETE_USER,
  GET_USER,
  SEARCH_USER,
  PAGINATED_USERS,
  GET_USER_BY_ID,
  SET_SELECTED_USER_ID,
} from "../constant/adminConstant";

export const getUser = () => (dispatch) => {
  adminService
    .getUser()
    .then((res) => {
      dispatch({
        type: GET_USER,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const deleteUser = (id) => (dispatch) => {
  adminService
    .deleteUser(id)
    .then(() => {
      dispatch({
        type: DELETE_USER,
        payload: id,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
// Thêm action setSelectedUserId để cập nhật selectedUserId
export const setSelectedUserId = (id) => (dispatch) => {
  dispatch({
    type: SET_SELECTED_USER_ID,
    payload: id,
  });
};

export const getUserById = (id) => (dispatch) => {
  adminService
    .getUserById(id)
    .then((res) => {
      dispatch({
        type: GET_USER_BY_ID,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const setAddUser = (userData) => {
  return (dispatch) => {
    adminService
      .addUser(userData)
      .then(() => {
        // Lấy lại danh sách người dùng từ backend và gọi dispatch để cập nhật dữ liệu
        adminService.getUser().then((res) => {
          dispatch({
            type: GET_USER,
            payload: res.data.content,
          });
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
export const searchUser = (keyword) => (dispatch) => {
  adminService
    .searchUser(keyword)
    .then((res) => {
      dispatch({
        type: SEARCH_USER,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const getPaginatedUsers = (pageIndex, pageSize) => (dispatch) => {
  adminService
    .getPaginatedUsers(pageIndex, pageSize)
    .then((res) => {
      dispatch({
        type: PAGINATED_USERS,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const putUserById = (id, userData) => (dispatch) => {
  adminService
    .putUserById(id, userData)
    .then(() => {
      // Lấy lại danh sách người dùng từ backend và gọi dispatch để cập nhật dữ liệu
      adminService.getUser().then((res) => {
        dispatch({
          type: GET_USER,
          payload: res.data.content,
        });
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
