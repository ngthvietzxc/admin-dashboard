import { roomService } from "./../../services/roomService";
import {
  DELETE_ROOM,
  GET_ROOM,
  GET_ROOM_BY_ID,
} from "./../constant/roomConstant";

export const getRoom = () => (dispatch) => {
  roomService
    .getRoom()
    .then((res) => {
      dispatch({
        type: GET_ROOM,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const deleteRoom = (id) => (dispatch) => {
  roomService
    .deleteRoom(id)
    .then(() => {
      dispatch({
        type: DELETE_ROOM,
        payload: id,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const setAddRoom = (roomData) => {
  return (dispatch) => {
    roomService
      .addRoom(roomData)
      .then(() => {
        // Lấy lại danh sách người dùng từ backend và gọi dispatch để cập nhật dữ liệu
        roomService.getRoom().then((res) => {
          dispatch({
            type: GET_ROOM,
            payload: res.data.content,
          });
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const getRoomById = (id) => (dispatch) => {
  roomService
    .getRoomById(id)
    .then((res) => {
      dispatch({
        type: GET_ROOM_BY_ID,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const putRoomById = (id, roomData) => (dispatch) => {
  roomService
    .putRoomById(id, roomData)
    .then(() => {
      // Lấy lại danh sách người dùng từ backend và gọi dispatch để cập nhật dữ liệu
      roomService.getRoom().then((res) => {
        dispatch({
          type: GET_ROOM,
          payload: res.data.content,
        });
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
