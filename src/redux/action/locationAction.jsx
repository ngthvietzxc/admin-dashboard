import { locationService } from "../../services/locationService";
import {
  DELETE_LOCATION,
  GET_LOCATION,
  GET_LOCATION_BY_ID,
  SET_SELECTED_LOCATION_ID,
} from "../constant/locationConstant";

export const getLocation = () => (dispatch) => {
  locationService
    .getLocation()
    .then((res) => {
      dispatch({
        type: GET_LOCATION,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const deleteLocation = (id) => (dispatch) => {
  locationService
    .deleteLocation(id)
    .then(() => {
      dispatch({
        type: DELETE_LOCATION,
        payload: id,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const setAddLocation = (locationData) => {
  return (dispatch) => {
    locationService
      .addLocation(locationData)
      .then(() => {
        // Lấy lại danh sách người dùng từ backend và gọi dispatch để cập nhật dữ liệu
        locationService.getLocation().then((res) => {
          dispatch({
            type: GET_LOCATION,
            payload: res.data.content,
          });
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
export const setSelectedLocationId = (id) => (dispatch) => {
  dispatch({
    type: SET_SELECTED_LOCATION_ID,
    payload: id,
  });
};

export const getLocationById = (id) => (dispatch) => {
  locationService
    .getLocationById(id)
    .then((res) => {
      dispatch({
        type: GET_LOCATION_BY_ID,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const putLocationById = (id, locationData) => (dispatch) => {
  locationService
    .putLocationById(id, locationData)
    .then(() => {
      // Lấy lại danh sách người dùng từ backend và gọi dispatch để cập nhật dữ liệu
      locationService.getLocation().then((res) => {
        dispatch({
          type: GET_LOCATION,
          payload: res.data.content,
        });
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
