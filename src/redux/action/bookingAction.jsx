import { bookingService } from "./../../services/bookingService";
import { GET_BOOKING } from "./../constant/bookingConstant";

export const getBooking = () => (dispatch) => {
  bookingService
    .getBooking()
    .then((res) => {
      dispatch({
        type: GET_BOOKING,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
